// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package config_api_impl

import (
	"gitcode.com/opendragonfly/df_proto_gen_go.git/config_api"
)

type ConfigApiImpl struct {
	config_api.UnimplementedConfigApiServer
}
