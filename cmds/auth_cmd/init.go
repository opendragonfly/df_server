// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package auth_cmd

import "github.com/spf13/cobra"

var AuthCmd = &cobra.Command{
	Use: "auth",
}

func init() {
	AuthCmd.AddCommand(statusCmd, enableCmd, disableCmd, listSecretCmd, addSecretCmd, updateSecretCmd, removeSecretCmd)
}
