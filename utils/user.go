// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package utils

import (
	"os/exec"
	"os/user"
	"strconv"
)

func InitUserAndGroup() error {
	_, err := user.LookupGroup(SYSTEM_GROUP)
	if err != nil {
		cmd := exec.Command("groupadd", SYSTEM_GROUP)
		err = cmd.Run()
		if err != nil {
			return err
		}
	}
	_, err = user.Lookup(SYSTEM_USER)
	if err != nil {
		cmd := exec.Command("useradd", "-g", SYSTEM_GROUP, "-M", SYSTEM_USER)
		err = cmd.Run()
		if err != nil {
			return err
		}
	}
	return nil
}

func GetUidAndGid() (int, int, error) {
	u, err := user.Lookup(SYSTEM_USER)
	if err != nil {
		return 0, 0, err
	}
	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		return 0, 0, err
	}
	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		return 0, 0, err
	}
	return uid, gid, nil
}
