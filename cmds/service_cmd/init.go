// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package service_cmd

import "github.com/spf13/cobra"

var ServiceCmd = &cobra.Command{
	Use: "service",
}

func init() {
	ServiceCmd.AddCommand(installCmd, runCmd, startCmd, stopCmd, statusCmd)
}
