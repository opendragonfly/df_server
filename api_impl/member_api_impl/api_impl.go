// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package member_api_impl

import (
	"context"

	"gitcode.com/opendragonfly/df_proto_gen_go.git/member_api"
	"gitcode.com/opendragonfly/df_server/config"
	"gitcode.com/opendragonfly/df_server/dao/member_dao"
	"github.com/dgraph-io/badger/v4"
)

type MemberApiImpl struct {
	member_api.UnimplementedMemberApiServer
}

func (apiImpl *MemberApiImpl) List(ctx context.Context, req *member_api.ListRequest) (*member_api.ListResponse, error) {
	emptyList := []*member_api.MemberInfo{}

	cfg, err := config.ReadServerConfigFromCache()
	if err != nil {
		return nil, err
	}
	remoteCfg, ok := cfg.GetRemote(req.RemoteId)
	if !ok {
		return &member_api.ListResponse{
			Code:       member_api.ListResponse_CODE_NO_REMOTE,
			ErrMsg:     "鉴权服务器不存在",
			MemberList: emptyList,
		}, nil
	}
	remoteMember, _, err := member_dao.QueryMember(ctx, remoteCfg.RemoteAddr, req.AccessToken)
	if err != nil {
		return &member_api.ListResponse{
			Code:       member_api.ListResponse_CODE_NO_PERMISSION,
			ErrMsg:     "用户不存在",
			MemberList: emptyList,
		}, nil
	}
	if !remoteMember.MemberIsAdmin {
		return &member_api.ListResponse{
			Code:       member_api.ListResponse_CODE_NO_PERMISSION,
			ErrMsg:     "没有权限",
			MemberList: emptyList,
		}, nil
	}
	memberList, err := member_dao.MemberDao.List(remoteCfg.ProjectId)
	if err != nil {
		return nil, err
	}

	return &member_api.ListResponse{
		Code:       member_api.ListResponse_CODE_OK,
		MemberList: memberList,
	}, nil
}

func (apiImpl *MemberApiImpl) Remove(ctx context.Context, req *member_api.RemoveRequest) (*member_api.RemoveResponse, error) {
	cfg, err := config.ReadServerConfigFromCache()
	if err != nil {
		return nil, err
	}
	remoteCfg, ok := cfg.GetRemote(req.RemoteId)
	if !ok {
		return &member_api.RemoveResponse{
			Code:   member_api.RemoveResponse_CODE_NO_REMOTE,
			ErrMsg: "鉴权服务器不存在",
		}, nil
	}
	remoteMember, _, err := member_dao.QueryMember(ctx, remoteCfg.RemoteAddr, req.AccessToken)
	if err != nil {
		return &member_api.RemoveResponse{
			Code:   member_api.RemoveResponse_CODE_NO_PERMISSION,
			ErrMsg: "用户不存在",
		}, nil
	}
	if !remoteMember.MemberIsAdmin {
		return &member_api.RemoveResponse{
			Code:   member_api.RemoveResponse_CODE_NO_PERMISSION,
			ErrMsg: "没有权限",
		}, nil
	}
	err = member_dao.MemberDao.Remove(remoteCfg.ProjectId, req.MemberUserId)
	if err != nil {
		return nil, err
	}

	return &member_api.RemoveResponse{
		Code: member_api.RemoveResponse_CODE_OK,
	}, nil
}

func (apiImpl *MemberApiImpl) UpdatePerm(ctx context.Context, req *member_api.UpdatePermRequest) (*member_api.UpdatePermResponse, error) {
	cfg, err := config.ReadServerConfigFromCache()
	if err != nil {
		return nil, err
	}
	remoteCfg, ok := cfg.GetRemote(req.RemoteId)
	if !ok {
		return &member_api.UpdatePermResponse{
			Code:   member_api.UpdatePermResponse_CODE_NO_REMOTE,
			ErrMsg: "鉴权服务器不存在",
		}, nil
	}
	remoteMember, _, err := member_dao.QueryMember(ctx, remoteCfg.RemoteAddr, req.AccessToken)
	if err != nil {
		return &member_api.UpdatePermResponse{
			Code:   member_api.UpdatePermResponse_CODE_NO_PERMISSION,
			ErrMsg: "用户不存在",
		}, nil
	}
	if !remoteMember.MemberIsAdmin {
		return &member_api.UpdatePermResponse{
			Code:   member_api.UpdatePermResponse_CODE_NO_PERMISSION,
			ErrMsg: "没有权限",
		}, nil
	}
	memberItem, err := member_dao.MemberDao.Get(remoteCfg.ProjectId, req.MemberUserId)
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return &member_api.UpdatePermResponse{
				Code:   member_api.UpdatePermResponse_CODE_NO_MEMBER,
				ErrMsg: "成员不存在",
			}, nil
		}
		return nil, err
	}
	memberItem.Perm = req.Perm
	err = member_dao.MemberDao.Set(remoteCfg.ProjectId, memberItem)
	if err != nil {
		return nil, err
	}
	return &member_api.UpdatePermResponse{
		Code: member_api.UpdatePermResponse_CODE_OK,
	}, nil
}

func (apiImpl *MemberApiImpl) GetMyPerm(ctx context.Context, req *member_api.GetMyPermRequest) (*member_api.GetMyPermResponse, error) {
	emptyInfo := &member_api.MemberPerm{}

	cfg, err := config.ReadServerConfigFromCache()
	if err != nil {
		return nil, err
	}
	remoteCfg, ok := cfg.GetRemote(req.RemoteId)
	if !ok {
		return &member_api.GetMyPermResponse{
			Code:   member_api.GetMyPermResponse_CODE_NO_REMOTE,
			ErrMsg: "鉴权服务器不存在",
			Perm:   emptyInfo,
		}, nil
	}
	_, localMember, err := member_dao.QueryMember(ctx, remoteCfg.RemoteAddr, req.AccessToken)
	if err != nil {
		return &member_api.GetMyPermResponse{
			Code:   member_api.GetMyPermResponse_CODE_NO_PERMISSION,
			ErrMsg: "用户不存在",
			Perm:   emptyInfo,
		}, nil
	}

	return &member_api.GetMyPermResponse{
		Code: member_api.GetMyPermResponse_CODE_OK,
		Perm: localMember.Perm,
	}, nil
}
