// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package service_cmd

import (
	"fmt"
	"os/exec"
	"os/user"

	"github.com/spf13/cobra"
)

var stopCmd = &cobra.Command{
	Use: "stop",
	RunE: func(cmd *cobra.Command, args []string) error {
		//检查是否是root用户
		curUser, err := user.Current()
		if err != nil {
			return err
		}
		if curUser.Uid != "0" {
			return fmt.Errorf("need root to run this command")
		}
		//调用systemctl
		stopCmd := exec.Command("/usr/bin/systemctl", "stop", "dragonfly")
		err = stopCmd.Run()
		if err != nil {
			return err
		}
		fmt.Println("stop success")
		return nil
	},
}
