// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package member_dao

import (
	"path/filepath"

	"gitcode.com/opendragonfly/df_server/utils"
	"github.com/dgraph-io/badger/v4"
)

var MemberDao *_MemberDao

var _globalDbh *badger.DB

func Init() error {
	dbPath := filepath.Join(utils.DATA_DIR_PATH, "member")
	options := badger.DefaultOptions(dbPath)
	dbh, err := badger.Open(options)
	if err != nil {
		return err
	}
	_globalDbh = dbh
	MemberDao = &_MemberDao{dbh: dbh}
	return nil
}

func Close() {
	if _globalDbh != nil {
		_globalDbh.Close()
		_globalDbh = nil
	}
}
