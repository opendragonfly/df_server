// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package dao

import (
	"gitcode.com/opendragonfly/df_server/dao/config_dao"
	"gitcode.com/opendragonfly/df_server/dao/member_dao"
	"gitcode.com/opendragonfly/df_server/dao/trace_dao"
)

func Init(keepTraceDay uint) (err error) {
	err = config_dao.Init()
	if err != nil {
		return
	}
	err = member_dao.Init()
	if err != nil {
		return
	}
	err = trace_dao.Init(keepTraceDay)
	if err != nil {
		return
	}
	return
}

func Close() {
	config_dao.Close()
	member_dao.Close()
	trace_dao.Close()
}
