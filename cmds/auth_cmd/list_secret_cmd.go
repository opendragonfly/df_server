// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package auth_cmd

import (
	"fmt"

	"gitcode.com/opendragonfly/df_proto_gen_go.git/config_api"
	"gitcode.com/opendragonfly/df_server/config"
	"gitcode.com/opendragonfly/df_server/utils"
	"github.com/spf13/cobra"
)

var listSecretCmd = &cobra.Command{
	Use: "listSecret",
	RunE: func(cmd *cobra.Command, args []string) error {
		cfg, err := config.ReadServerConfig()
		if err != nil {
			return err
		}
		if cfg.SuperToken == "" {
			return fmt.Errorf("miss super token in config")
		}
		conn, err := utils.ConnGrpcServer(fmt.Sprintf("127.0.0.1:%d", cfg.Port.ServicePort))
		if err != nil {
			return err
		}
		defer conn.Close()

		client := config_api.NewConfigApiClient(conn)
		res, err := client.ListAuthSecret(cmd.Context(), &config_api.ListAuthSecretRequest{
			AccessToken: cfg.SuperToken,
		})
		if err != nil {
			return err
		}
		if res.Code != config_api.ListAuthSecretResponse_CODE_OK {
			return fmt.Errorf("%s", res.ErrMsg)
		}
		fmt.Println("auth secret count", len(res.AuthSecretList))
		for _, secret := range res.AuthSecretList {
			fmt.Println(secret.Username, secret.Password)
		}
		return nil
	},
}
