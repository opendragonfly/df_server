// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package member_dao

import (
	"fmt"

	"gitcode.com/opendragonfly/df_proto_gen_go.git/member_api"
	"github.com/dgraph-io/badger/v4"
	"google.golang.org/protobuf/proto"
)

type _MemberDao struct {
	dbh *badger.DB
}

func (db *_MemberDao) genKeyPrefix(projectId string) string {
	return fmt.Sprintf("%s:", projectId)
}

func (db *_MemberDao) genKey(projectId, memberUserId string) string {
	return fmt.Sprintf("%s:%s", projectId, memberUserId)
}

func (db *_MemberDao) List(projectId string) ([]*member_api.MemberInfo, error) {
	txn := db.dbh.NewTransaction(false)
	defer txn.Discard()

	keyPrefix := db.genKeyPrefix(projectId)

	options := badger.IteratorOptions{
		PrefetchValues: true,
		PrefetchSize:   100,
		Reverse:        false,
		AllVersions:    false,
		Prefix:         []byte(keyPrefix),
	}
	iter := txn.NewIterator(options)
	defer iter.Close()

	retList := []*member_api.MemberInfo{}

	for iter.Rewind(); iter.Valid(); iter.Next() {
		item := iter.Item()
		if item.IsDeletedOrExpired() {
			continue
		}
		value, err := item.ValueCopy(nil)
		if err != nil {
			return nil, err
		}
		info := &member_api.MemberInfo{}
		err = proto.Unmarshal(value, info)
		if err != nil {
			return nil, err
		}
		retList = append(retList, info)
	}

	return retList, nil
}

func (db *_MemberDao) Set(projectId string, member *member_api.MemberInfo) error {
	txn := db.dbh.NewTransaction(true)
	defer txn.Discard()

	key := db.genKey(projectId, member.MemberUserId)
	value, err := proto.Marshal(member)
	if err != nil {
		return err
	}
	err = txn.Set([]byte(key), value)
	if err != nil {
		return err
	}
	return txn.Commit()
}

func (db *_MemberDao) Get(projectId, memberUserId string) (*member_api.MemberInfo, error) {
	txn := db.dbh.NewTransaction(false)
	defer txn.Discard()

	key := db.genKey(projectId, memberUserId)

	item, err := txn.Get([]byte(key))
	if err != nil {
		return nil, err
	}
	if item.IsDeletedOrExpired() {
		return nil, badger.ErrKeyNotFound
	}
	value, err := item.ValueCopy(nil)
	if err != nil {
		return nil, err
	}
	info := &member_api.MemberInfo{}
	err = proto.Unmarshal(value, info)
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (db *_MemberDao) Remove(projectId, memberUserId string) error {
	txn := db.dbh.NewTransaction(true)
	defer txn.Discard()

	key := db.genKey(projectId, memberUserId)
	err := txn.Delete([]byte(key))
	if err != nil {
		return err
	}
	return txn.Commit()
}
