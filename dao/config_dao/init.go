// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package config_dao

import (
	"path/filepath"

	"gitcode.com/opendragonfly/df_server/utils"
	"github.com/dgraph-io/badger/v4"
)

var StateDao *_StateDao
var SecretDao *_SecretDao

var _globalDbh *badger.DB

func Init() error {
	dbPath := filepath.Join(utils.DATA_DIR_PATH, "auth")
	options := badger.DefaultOptions(dbPath)
	dbh, err := badger.Open(options)
	if err != nil {
		return err
	}
	_globalDbh = dbh
	StateDao = &_StateDao{dbh: dbh}
	SecretDao = &_SecretDao{dbh: dbh}
	return nil
}

func Close() {
	if _globalDbh != nil {
		_globalDbh.Close()
		_globalDbh = nil
	}
}
