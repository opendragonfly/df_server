// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package config_cmd

import (
	"fmt"

	"gitcode.com/opendragonfly/df_server/config"
	"github.com/spf13/cobra"
)

var listRemoteCmd = &cobra.Command{
	Use: "listRemote",
	RunE: func(cmd *cobra.Command, args []string) error {
		cfg, err := config.ReadServerConfig()
		if err != nil {
			return err
		}
		for _, remoteCfg := range cfg.RemoteList {
			fmt.Printf("remoteId:%s,projectId:%s,remoteAddr:%s\n", remoteCfg.RemoteId, remoteCfg.ProjectId, remoteCfg.RemoteAddr)
		}
		return nil
	},
}
