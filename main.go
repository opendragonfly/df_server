// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package main

import (
	"fmt"
	"os"

	"gitcode.com/opendragonfly/df_server/cmds/auth_cmd"
	"gitcode.com/opendragonfly/df_server/cmds/config_cmd"
	"gitcode.com/opendragonfly/df_server/cmds/service_cmd"
	"gitcode.com/opendragonfly/df_server/cmds/trace_cmd"
	"github.com/spf13/cobra"
)

const Version = "0.1.1"

var versionCmd = &cobra.Command{
	Use: "version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("OpenDragonFly", Version)
	},
}

func main() {
	var rootCmd = &cobra.Command{}
	rootCmd.AddCommand(config_cmd.ConfigCmd, service_cmd.ServiceCmd, auth_cmd.AuthCmd, trace_cmd.TraceCmd, versionCmd)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
