// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package service_cmd

import (
	"fmt"
	"os"
	"os/exec"
	"os/user"

	"github.com/spf13/cobra"
)

var statusCmd = &cobra.Command{
	Use: "status",
	RunE: func(cmd *cobra.Command, args []string) error {
		//检查是否是root用户
		curUser, err := user.Current()
		if err != nil {
			return err
		}
		if curUser.Uid != "0" {
			return fmt.Errorf("need root to run this command")
		}
		//调用systemctl
		statusCmd := exec.Command("/usr/bin/systemctl", "status", "dragonfly")
		statusCmd.Stdin = os.Stdin
		statusCmd.Stdout = os.Stdout
		statusCmd.Stderr = os.Stderr
		err = statusCmd.Run()
		if err != nil {
			return err
		}
		return nil
	},
}
