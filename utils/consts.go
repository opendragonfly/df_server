// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package utils

const CONFIG_FILE_PATH = "/etc/dragonfly.yaml"
const SERVICE_FILE_PATH = "/usr/lib/systemd/system/dragonfly.service"
const DATA_DIR_PATH = "/var/lib/dragonfly"
const PID_FILE_PATH = "/var/lib/dragonfly/dragonfly.pid"

const SYSTEM_USER = "dragonfly"
const SYSTEM_GROUP = "dragonfly"

const EXEC_FILE_PATH = "/usr/sbin/dragonfly"
