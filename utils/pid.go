// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package utils

import (
	"fmt"
	"os"
	"strconv"
)

func StorePid() error {
	pid := os.Getpid()
	pidStr := fmt.Sprintf("%d", pid)
	return os.WriteFile(PID_FILE_PATH, []byte(pidStr), 0644)
}

func LoadPid() (int, error) {
	data, err := os.ReadFile(PID_FILE_PATH)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(string(data))
}
