// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package trace_cmd

import "github.com/spf13/cobra"

var TraceCmd = &cobra.Command{
	Use: "trace",
}

func init() {
	TraceCmd.AddCommand(listServiceCmd, listRootSpanNameCmd, listTraceCmd, getTraceCmd, listSpanCmd)
}
