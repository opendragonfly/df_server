// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package auth_cmd

import (
	"fmt"

	"gitcode.com/opendragonfly/df_proto_gen_go.git/config_api"
	"gitcode.com/opendragonfly/df_server/config"
	"gitcode.com/opendragonfly/df_server/utils"
	"github.com/spf13/cobra"
)

var addSecretCmd = &cobra.Command{
	Use: "addSecret",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 2 {
			return fmt.Errorf("need param [username] [password]")
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		cfg, err := config.ReadServerConfig()
		if err != nil {
			return err
		}
		if cfg.SuperToken == "" {
			return fmt.Errorf("miss super token in config")
		}
		conn, err := utils.ConnGrpcServer(fmt.Sprintf("127.0.0.1:%d", cfg.Port.ServicePort))
		if err != nil {
			return err
		}
		defer conn.Close()

		username := args[0]
		password := args[1]

		client := config_api.NewConfigApiClient(conn)
		res, err := client.AddAuthSecret(cmd.Context(), &config_api.AddAuthSecretRequest{
			AccessToken: cfg.SuperToken,
			AuthSecret: &config_api.AuthSecretInfo{
				Username: username,
				Password: password,
			},
		})
		if err != nil {
			return err
		}
		if res.Code != config_api.AddAuthSecretResponse_CODE_OK {
			return fmt.Errorf("%s", res.ErrMsg)
		}
		fmt.Println("add auth secret success")
		return nil
	},
}
