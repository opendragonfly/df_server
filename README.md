# 简介

OpenDragonFly是一个易于安装和使用的链路追踪管理系统，它可以接收来自zipkin,jaeger,skywalking和otlp协议的链路追踪数据。此外还支持了：
- 数据入口权限验证(目前只支持Basic Auth)
- 成员权限管理
- 命令行工具
- OpenLinkSaas图像管理界面

![arch](/images/arch.png)

# 安装和启动

## 安装

1. 从[这里](https://gitcode.com/opendragonfly/df_server/releases)下载最新的程序
2. 在linux终端下面运行如下命令
   
   ```bash
   # ./df_server service install
   ```
   
   运行这个命令后会把df_server复制到/usr/sbin/dragonfly，并初始化配置/etc/dragonfly.yaml，创建数据目录/var/lib/dragonfly。最后生成/usr/lib/systemd/system/dragonfly.service

## 启动服务

你可以用systemctl start dragonfly或/usr/sbin/dragonfly service start来启动服务。
用systemctl status dragonfly或/usr/sbin/dragonfly service status来确认启动状态。

# 管理数据入库权限

## 打开关闭入库数据检查

你可以运行/usr/sbin/dragonfly auth status来确认是否打开了入库数据检查。
如果你要打开入库数据验证，运行/usr/sbin/dragonfly auth enable即可
如果你要关闭入库数据验证，运行/usr/sbin/dragonfly auth disable即可

## 管理入库验证密钥

入库验证目前支持Basic Auth，密钥由用户名和密码组成。

- /usr/sbin/dragonfly auth listSecret   查看现有密钥
- /usr/sbin/dragonfly auth addSecret    增加密钥
- /usr/sbin/dragonfly auth updateSecret 修改密钥
- /usr/sbin/dragonfly auth removeSecret 删除密钥

# 查看链路追踪数据

一个链路追踪数据由服务名称，服务版本和链路追踪ID标识，并携带多个链路追踪数据片段(Span)。
通过命令行，可以获取本地dragonfly服务上的数据：

- /usr/sbin/dragonfly trace listService 列出服务名称和版本
- /usr/sbin/dragonfly trace listRootSpanName 列出Top基本的链路追踪数据片段名称
- /usr/sbin/dragonfly trace listTrace 列出指定排序的链路追踪数据
- /usr/sbin/dragonfly trace getTrace 获取指定链路追踪ID的数据
- /usr/sbin/dragonfly trace listSpan 列出指定链路追踪ID相关的所有链路追踪数据片段

# 使用OpenLinkSaas图形界面

[OpenLinkSaas](https://gitcode.com/linksaas/desktop/)作为研发效能一站式解决方案，提供了OpenDragonFly的图形管理界面。
你可以按如下操作，使用图形界面管理链路追踪数据。

## 加入到OpenLinkSaas项目中
![add in openlinksaas](./images/add_in_openlinksaas.png)

在完成上述操作后，会在项目列表中看到对应的opendragonfly服务入口。

![entry in openlinksaas](./images/entry_in_openlinksaas.png)

## 查看端口信息
![port info](./images/port_info.png)

## 管理入库验证密钥

![manage secret](./images/manage_secret.png)

## 查看链路数据

![trace list](./images/trace_list.png)