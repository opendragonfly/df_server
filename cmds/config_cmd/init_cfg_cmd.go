// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package config_cmd

import (
	"fmt"
	"os"
	"os/user"

	"gitcode.com/opendragonfly/df_server/config"
	"gitcode.com/opendragonfly/df_server/utils"
	"github.com/dchest/uniuri"
	"github.com/spf13/cobra"
)

var initCfgCmd_Force bool

var initCfgCmd = &cobra.Command{
	Use: "init",
	RunE: func(cmd *cobra.Command, args []string) error {
		//检查是否是root用户
		curUser, err := user.Current()
		if err != nil {
			return err
		}
		if curUser.Uid != "0" {
			return fmt.Errorf("need root to run this command")
		}

		_, err = os.Stat(utils.CONFIG_FILE_PATH)
		if err == nil && !initCfgCmd_Force {
			return fmt.Errorf("config file exist,please use --force flag")
		}
		cfg := &config.ServerConfig{
			KeepTraceDay: 7,
			SuperToken:   uniuri.NewLenChars(32, []byte("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")),
			Port: config.PortConfig{
				ServicePort:          7000,
				EnableZipkin:         true,
				ZipkinPort:           9411,
				EnableJaegerGrpc:     true,
				JaegerGrpcPort:       14250,
				EnableJaegerHttp:     true,
				JaegerHttpPort:       14268,
				EnableSkywalkingGrpc: true,
				SkywalkingGrpcPort:   11800,
				EnableSkywalkingHttp: true,
				SkywalkingHttpPort:   12800,
				EnableOtlpGrpc:       true,
				OtlpGrpcPort:         4317,
				EnableOtlpHttp:       true,
				OtlpHttpPort:         4318,
			},
			RemoteList: []*config.RemoteConfig{},
		}
		err = config.WriteServerConfig(cfg)
		if err != nil {
			return err
		}
		fmt.Println("init config success")
		return nil
	},
}

func init() {
	initCfgCmd.Flags().BoolVar(&initCfgCmd_Force, "force", false, "force init config")
}
