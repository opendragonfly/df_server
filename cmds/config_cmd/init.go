// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package config_cmd

import "github.com/spf13/cobra"

var ConfigCmd = &cobra.Command{
	Use: "config",
}

func init() {
	ConfigCmd.AddCommand(initCfgCmd, addRemoteCmd, listRemoteCmd, removeRemoteCmd, resetSuperTokenCmd)
}
